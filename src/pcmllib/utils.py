import os, ctypes

# JAVA

def check_java_version(path):
    # 判断是不是 Java/bin 目录
    if os.path.isdir(path):
        if os.path.isfile(os.path.join(path, 'java.exe')) and \
        os.path.isfile(os.path.join(path,'javaw.exe')) and \
        os.path.isfile(os.path.join(path,'jar.exe')):
            
            #if ctypes.windll.shell32.IsUserAnAdmin() == 1:
                # 管理员权限
                root_dir = os.path.abspath(os.path.join(path, ".."))
                with open(os.path.join(root_dir, 'release'), mode='r') as fd:
                    javaver = fd.read().split('\n')                         # Goto ['xx = "xx"', 'xx = "xx"', ...]
                for i in javaver:
                    if i.find('JAVA_VERSION="') != -1:
                        javaver = i                                         # Goto 'JAVA_VERSION="x.x.x_xxx"'
                javaver = javaver.lstrip('JAVA_VERSION="').rstrip('"')      # Goto 'x.x.x_xxx'
                javaver = javaver.split(".")                                # Goto tuple ['x', 'x', 'x(_xxx)']
                
                if javaver[0] == '1':
                    return javaver[1]
                else:
                    return javaver[0]
            #else:
            #    # Not Tested, Require pywin32
            #    from win32com.client import Dispatch
            #    parser = Dispatch("Scripting.FileSystemObject")
            #    javaver = parser.GetFileVersion(os.path.join(path, "java.exe"))
            #    javaver = javaver.split(".")
                
                return javaver[0]
    return None

def get_java_versions():
    jres = {}
    
    sys_path = os.environ.get("PATH").split(";")
    for p in sys_path:
        ver = check_java_version(p)
        if not ver is None:
            if jres.get(ver) is None:
                jres[ver] = p
            else:
                list = []
                list.append(jres[ver])
                list.append(p)
                jres[ver] = list

    return jres
   