import pooch

logger = pooch.get_logger()
logger.setLevel("WARNING")


def download(url: str, hash: str | None = None):
    # TODO: Use rich progress to remove tqdm
    path = pooch.retrieve(
        url=url,
        known_hash=hash,
        progressbar=True
    )
    return path
    