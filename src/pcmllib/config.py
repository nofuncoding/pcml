import os
from pcmllib import utils
from pcmllib.console import console, log
from rich import print
from rich.prompt import Prompt, Confirm

try:
    import tomllib
except ImportError:
    import tomli as tomllib

import tomli_w

CFG_FOLDERPATH = os.path.abspath("./config")
CFG_LAUNCHERPATH = os.path.join(CFG_FOLDERPATH, "launcher_cfg.toml")


def _create_config():
    if not os.path.isdir(CFG_FOLDERPATH):
        os.mkdir(CFG_FOLDERPATH)
    if not os.path.isfile(CFG_LAUNCHERPATH):
        with open(CFG_LAUNCHERPATH, mode="wb"):
            return

def can_decode() -> bool:
    _create_config()
    try:
        with open(CFG_LAUNCHERPATH, mode="rb") as f:
            tomllib.load(f)
    except tomllib.TOMLDecodeError:
        return False
    return True

def is_exists() -> bool:
    if os.path.isfile(CFG_LAUNCHERPATH):
        return True
    else:
        return False

def is_initiated() -> bool:
    if not is_exists():
        return False
    if os.path.getsize(CFG_LAUNCHERPATH) <= 0:
        return False
    return True
    
def first_configure():
    if is_initiated():
        return

    print("Starting first configure >>> \n")

    while True:
        done_cfg = {}

        # Ask for the game path
        while True:
            gp = Prompt.ask("Where will Minecraft be install")
            if not os.path.isabs(gp):
                print("[bold red]ERROR[/bold  red]: This input is not a absolute path.")
                continue
            if not os.path.isdir(gp):
                print("[bold yellow]WARNING[/bold  yellow]: This input is not a directory.")
                if not Confirm.ask("Do you want to create it?", default=False):
                    continue
                else:
                    os.makedirs(gp)
            if not Confirm.ask(f"Do you really want to install to {gp}?"):
                continue
            done_cfg["game_path"] = gp
            break
                
        # Check for java paths
        jres = {}
        with console.status("Checking Installed Java Versions..."):
            jres = utils.get_java_versions()
        print("Found these java versions in %PATH%:", jres)
        done_cfg["java_path"] = jres

        # Select Mirror
        available_game_mirrors = ["mojang", "bmclapi", "mcbbs"]
        game_mirror = Prompt.ask("Which mirror do you want to use at downloading game?", choices=available_game_mirrors, default=available_game_mirrors[0])
        
        available_update_mirrors = ["github", "ghproxy"]
        update_mirror = Prompt.ask("Which mirror do you want to use at updating PCML?", choices=available_update_mirrors, default=available_update_mirrors[0])
        done_cfg["download"] = {"game_mirror": game_mirror, "update_mirror": update_mirror}

        # Final Confirm
        print("[green bold]Configure Completed![/green bold] Your config is:", done_cfg)
        if Confirm.ask("Confirm?", default=True):
            write_all(done_cfg)
            return

def get_obj():
    with open(CFG_LAUNCHERPATH, mode="rb") as f:
        return tomllib.load(f)

def write_all(d: dict):
    with open(CFG_LAUNCHERPATH, mode="wb") as f:
        log.info("Writing config file")
        tomli_w.dump(d, f)