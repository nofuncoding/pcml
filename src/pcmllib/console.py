from rich.console import Console
from rich.logging import RichHandler
import logging


log_level = logging.DEBUG

logging.basicConfig(
    level=log_level,
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True)]
)

log = logging.getLogger("rich")
console = Console()