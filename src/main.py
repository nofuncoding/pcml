from pcmllib import pcml_dl, config, __version__
from pcmllib.console import console, log

def main():
    # Test config
    if not config.can_decode():
        raise Exception("Could not read config:", config.CFG_LAUNCHERPATH)
    
    # First Configure
    config.first_configure()
    
    log.info(f"Reading config from {config.CFG_LAUNCHERPATH}")
    cfg = config.get_obj()


if __name__ == "__main__":
    main()