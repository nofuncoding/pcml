nuitka = python -m nuitka
default_args = --standalone --mingw64 --output-dir=out --show-progress --follow-imports -o pcml.exe
main = src/main.py

build:
	$(nuitka) $(default_args) $(main)

onefile:
	$(nuitka) $(default_args) --onefile $(main)