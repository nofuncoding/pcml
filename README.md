# PCML

A Minecraft Launcher based on Python CLI

## Install

To use it, you need **a terminal that supports ConPPY**. We recommend to use *Windows Terminal* or *Alacritty* (you also can use cmd, but it's so ugly ;)

Download the archive from Releases and extract to path with **NO SPECIAL CHARACTERS**,

*And then add it to `%PATH%`* (optional)

## Build

Requirements:

- Mingw64
- Python 3.8+
- Installed these pypi packages:
  - nuitka >= 1.7.0
  - pooch >= 1.7.0
  - rich >= 13.0.0
  - tqdm >= 4.65.0
  - tomli >= 2.0.0; *python_version < "3.11"*
  - tomli-w >= 1.0.0

Clone this repo using `git clone`, Then run `make` in project folder

*(If you want build PCML to a single file, use `make onefile`*

## Usage

Run the program, then setup it. After that, you can use it happily ;)